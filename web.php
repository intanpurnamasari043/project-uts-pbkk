<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::get('profile', function () {
    return view('UTS.create021180078');

});

Route:: post ('view021180078', function() {
    $npm = $_POST['npm'];
    $nama = $_POST['nama'];
    $prodi = $_POST['prodi'];
    $no_hp = $_POST['no_hp'];
    $tl = $_POST['tl'];
    $jk = $_POST['jk'];
    $agama = $_POST['agama'];




    return view('UTS.view021180078', ['npm' => $npm, 'nama' => $nama,
        'prodi' => $prodi, 'no_hp' => $no_hp, 'tl' => $tl,
        'jk' => $jk, 'agama' => $agama]);
});

