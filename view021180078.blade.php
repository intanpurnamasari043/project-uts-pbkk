<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Data</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">

</head>

<body>
    <br><br><br><br>
    <center>
        <h2> TAMPIL DATA DIRI</h2>
    </center>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                    <tr class="table-info">
                        <th scope="col">NPM</th>
                        <th scope="col">Nama</th>
                        <th scope="col">Program Studi</th>
                        <th scope="col">No Hp</th>
                        <th scope="col">Tempat, Tanggal Lahir</th>
                        <th scope="col">Jenis Kelamin</th>
                        <th scope="col">Agama</th>

                    </tr>
                </thead>
                <tbody>

                    <tr>

                        <td>{{ $npm }}</td>
                        <td>{{ $nama }}</td>
                        <td>{{ $prodi }}</td>
                        <td>{{ $no_hp }}</td>
                        <td>{{ $tl}}</td>
                        <td>{{ $jk }}</td>
                        <td>{{$agama }}</td>

                    </tr>
                    </tr>
                </tbody>
            </table>


</body>

</html>
